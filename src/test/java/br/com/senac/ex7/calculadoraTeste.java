/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex7;

import br.com.senac.ex7.Calculadora;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class calculadoraTeste {
    
     @Test
    public void DeveEstarAprovado() {
        double nota  = 8 ;
        Calculadora calculadora = new Calculadora();
        String resultado  = calculadora.calcular(nota);
        assertEquals(Calculadora.APROVADO, resultado);
          
    }
    
    @Test
    public void deveSerEleitor() {
        double nota  = 5.5 ;
        Calculadora calculadora = new Calculadora();
        String resultado  = calculadora.calcular(nota);
        assertEquals(Calculadora.RECUPERACAO, resultado);
          
    }
    
    @Test
    public void deveSerEleitorFacultativoJovem() {
        double nota  = 3.5 ;
        Calculadora calculadora = new Calculadora();
        String resultado  = calculadora.calcular(nota);
        assertEquals(Calculadora.REPROVADO, resultado);
          
    }
}
